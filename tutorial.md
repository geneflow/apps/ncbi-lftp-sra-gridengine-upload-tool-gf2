# NCBI SRA batch upload tool

Uploading to the NCBI site at times can be very cumbersome. To simpilify this process the SCBS team has developed a batch upload tool to alivate uploading multiple files individually, lose of connection, and etc. The process is as follows:
1. Create/login to scicomp
2. Create/login to NCBI
3. Retrieve ftp credentials from NCBI
4. Install SRA batch upload tool GeneFlow workflow
5. Upload files
6. Return to the NCBI and select you preload folder. This folder can be load before or after a submission has started.

#### Current file types supported:
- .fastq, .fastq.gz, .fq, .fq.gz, .bam, .fast5, bas.h5, .bax.h5, hdf5

## Create a scicomp account
The first step in this process is to create a Scicomp account. This can be done by going to the OAMD SCBS website found at [info.biotech.cdc.gov](https://info.biotech.cdc.gov/info/) 

<img src="./images/scicomp_select.png" width=700></img>

Enter your name and email address.

<img src="./images/scicomp_account.png" width=700></img>

Some jobs may take a while to complete so it's suggested to submit them to the HPC nodes on biolinux or apsen. For this reason we suggest that you request access to the **Advanced** services. However, this is not requested. At a minimum an **intermediate** account is needed. 

<img src="./images/scicomp_account_level.png" width=700></img>

## NCBI account creation
If you don't currently have an NCBI account you will have to create one by going to [](). We suggest that you create an account for you group, to do so you will need a new email from the CDC ITSO team. 

<img src="./images/NCBI_account_creation.png" width=700></img>

Once you have a NCBI account and are logged in, you can preload a folder using the ftp instructions. 

<img src="./images/select_ftp.png" width=700></img>

From the FTP instructions you will need your account folder (2). This is typically your email address followed by a random string. Do **not** include "uploads/". So in the example below you would need to use "qdy1_cdc.gov_XXXXXX". You do not have to name your folder "new_folder", we suggest that you give a more descriptive name incase you have multiple projects to upload in the same time frame.

<img src="./images/ncbi_upload_details.png" width=700></img>

Once you have the FTP creditials you can login to biolinux or aspen to install and run the SRA batch upload geneflow app. We suggest you submit to the submission queue or use screen to run the process in the background. The instructions are as follows:
screen -S upload

module load geneflow/2.0.0-alpha.3

gf install-workflow -g https://git.biotech.cdc.gov/qdy1/ncbi-lftp-sra-gridengine-upload-workflow-gf2.git -c --make_apps ./ncbi_sra_ge_tool -f 

gf run ./ncbi_sra_ge_tool/ -o ./output -n upload-job --in.reads=path/to/your/files --param.output_dir=new_folder_on_ncbi --param.remote=qdy1_cdc.gov_XXXXXX --ec default:local  --ep default.slots:1 --em default:environment
then exit the screen with ctrl+a+d
screen -r upload
Once the upload has complete you can return to the NCBI and select you preload folder. This folder can be load before or after a submission has started.

<img src="./images/preloaded_folder.png" width=700></img>

### All done
